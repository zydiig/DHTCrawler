import socket
import traceback
from hashlib import sha1
from io import BytesIO
from queue import Queue
from socket import inet_ntoa
from struct import unpack, pack
from threading import Timer, Thread
from time import sleep, time

from Crypto import Random

from bencoding import bencode, bdecode

DIGITS = [str(x).encode('utf-8') for x in range(0, 10)]

BOOTSTRAP_NODES = (
    ("router.bittorrent.com", 6881),
    ("dht.transmissionbt.com", 6881),
    ("router.utorrent.com", 6881)
)

TID_LENGTH = 2
REJOIN_DHT_INTERVAL = 3
TOKEN_LENGTH = 2

DHT_PORT = 6884

HANDSHAKE_PREFIX = b'\x13BitTorrent protocol\x00\x00\x00\x00\x00\x10\x00\x04'

BLOCK_SIZE = 2 ** 14

requests = Queue()
PEER_ID = Random.get_random_bytes(20)


def entropy(length):
    return Random.get_random_bytes(length)


def random_id():
    return entropy(20)


def decode_nodes(nodes):
    n = []
    length = len(nodes)
    if (length % 26) != 0:
        return n

    for i in range(0, length, 26):
        nid = nodes[i:i + 20]
        ip = inet_ntoa(nodes[i + 20:i + 24])
        port = unpack("!H", nodes[i + 24:i + 26])[0]
        n.append((nid, ip, port))
    return n


def timer(t, f):
    Timer(t, f).start()


def get_neighbor(target, nid, end=10):
    return target[:end] + nid[end:]


class Bitfield:
    def __init__(self, data: bytes):
        l = []
        for byte in data:
            binstr = bin(byte)[2:]
            for _ in range(8 - len(binstr)):
                l.append(0)
            for bit in binstr:
                l.append(int(bit))
        self.data = l

    def __getitem__(self, item):
        return self.data[item]

    def __iter__(self):
        for i in range(len(self.data)):
            yield self[i]

    def __bytes__(self):
        s = []
        for i in range(0, len(self.data), 8):
            v = sum([2 ** (7 - index) * bit for index, bit in enumerate(self.data[i:i + 8])])
            s.append(pack("B", v))
        return b''.join(s)


class Nodes:
    def __init__(self, max_size=200):
        self.l = []
        self.max_size = max_size

    def append(self, o):
        self.l.append(o)
        if len(self.l) > self.max_size:
            for x in sorted(self.l, key=lambda x: x.last_contacted):
                if x.last_contacted == -1:
                    continue
                self.l.remove(x)
                if len(self.l) <= self.max_size:
                    break

    def __len__(self):
        return len(self.l)

    def __iter__(self):
        return iter(self.l)

    def __contains__(self, item):
        for x in self.l:
            if x.ip == item:
                return True
        return False

    def __getitem__(self, item):
        for x in self.l:
            if x.ip == item:
                return x
        return None


class Request:
    def __init__(self, infohash, source):
        self.infohash, self.source = infohash, source
        self.fails = 0


class Node:
    def __init__(self, nid, ip, port, last_contacted=-1):
        self.nid = nid
        self.ip = ip
        self.port = port
        self.last_contacted = last_contacted


class BadClientError(Exception):
    pass

class ProtocolError(Exception):
    pass


class PeerWire:
    def __init__(self, address, infohash, peer_id):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(5)
        self.socket.connect(address)
        self.infohash, self.peer_id = infohash, peer_id

    def handshake(self):
        self.socket.sendall(HANDSHAKE_PREFIX + self.infohash + self.peer_id)
        resp = self.socket.recv(68)
        if resp[:20] != HANDSHAKE_PREFIX[:20]:
            raise BadClientError("Malformed protocol header {}".format(repr(resp)))
        elif resp[25] & 0x10 == 0:
            raise BadClientError("Client doesn't support extended protocol")
        elif resp[27] & 0x04 == 0:
            raise BadClientError("Client doesn't support fast protocol")

    def extended_handshake(self):
        data = b'\x14\x00' + bencode({"m": {"ut_metadata": 2}, "v": "Insight of DHT"})
        self.send_message(data)
        msg = self.recv_message()
        if msg[0] == 20 and msg[1] == 0:
            d = bdecode(msg[2:])
            return d
        else:
            return None

    def send_message(self, msg):
        data = pack('>I', len(msg)) + msg
        self.socket.sendall(data)

    def recv_message(self):
        data = self.socket.recv(4)
        if len(data) != 4:
            raise ProtocolError("Incomplete data. no length")
        length = unpack('>I', data)[0]
        bio = BytesIO()
        total = 0
        while True:
            size = length - total if length - total < 4096 else 4096
            chunk = self.socket.recv(size)
            bio.write(chunk)
            total += len(chunk)
            if total == length:
                return bio.getvalue()

    def close(self):
        self.socket.close()


class MetadataFetcher(Thread):
    def __init__(self):
        Thread.__init__(self)

    def send_pieces_request(self, pw, ut_metadata, pieces_count):
        for i in range(pieces_count):
            bio = BytesIO()
            bio.write(b'\x14' + pack("B", ut_metadata))
            bio.write(bencode({"msg_type": 0, "piece": i}))
            pw.send_message(bio.getvalue())

    def run(self):
        while True:
            req = requests.get()
            try:
                wire = PeerWire(req.source, req.infohash, PEER_ID)
                wire.handshake()
                d = wire.extended_handshake()
                wire.send_message(b'\x0F')
                wire.send_message(b'\x00')
                wire.send_message(b'\x03')
                if not d:
                    raise BadClientError("Unsupported peer client")
                if b'metadata_size' not in d:
                    raise ValueError("Client does not hava metadata")
                metadata_type = d[b'm'][b'ut_metadata']
                print("\033[01;36m", req.source[0], d[b'metadata_size'], "\033[00m")
                pieces_count = d[b'metadata_size'] // BLOCK_SIZE
                if d[b'metadata_size'] % BLOCK_SIZE != 0:
                    pieces_count += 1
                self.send_pieces_request(wire, d[b'm'][b'ut_metadata'], pieces_count)
                pieces = [None] * pieces_count
                while True:
                    try:
                        msg = wire.recv_message()
                    except ValueError as e:
                        print(e, d.get(b'v', "N/A"))
                        break
                    if msg == b'':
                        continue
                    bio = BytesIO(msg)
                    if bio.read(1)[0] == 20:
                        if bio.read(1)[0] == metadata_type:
                            d = bdecode(bio)
                            if d[b'msg_type'] == 1:
                                piece = bio.read()
                                pieces[d[b'piece']] = piece
                            else:
                                print("Failed to fetch {} from {}:{} -> piece {}".format(req.infohash, *req.source, d.get(b'piece', b'-1')))
                                break
                        else:
                            pass
                            # print("IGNORED BYTE1", msg[1],metadata_type)
                    else:
                        pass
                        # print("IGNORED BYTE0", msg[0], len(msg))
                    if all(pieces):
                        break
                if all(pieces):
                    metadata = b''.join(pieces)
                    h = sha1()
                    h.update(metadata)
                    if req.infohash == h.digest():
                        infod = bdecode(metadata)
                        if b'length' in infod:
                            print("\033[01;31m{}\033[00m".format(infod[b"name"]))
                        elif b'files' in infod:
                            print("\033[01;31m", end="")
                            for item in [{b'path':[infod[b"name"]]}]+infod[b'files']:
                                print(b"".join(item[b'path']), end=" ")
                            print("\n\033[00m", end="")
                    else:
                        print("\033[0;31m", "Metadata checksum failed", "\033[00m")
                    with open("mets", "a") as f:
                        f.write(req.infohash.hex() + " " + metadata.hex() + "\n")
                else:
                    print(pieces)
                    print('Incomplete metadata received')
            except (ConnectionRefusedError, socket.timeout, OSError) as e:
                #print("Connection failed", req.source[0])
                req.fails += 1
                if req.fails < 3:
                    requests.put(req)
                else:
                    print("Connection failed", req.source[0])
            except (ConnectionResetError, BadClientError,ProtocolError) as e:
                print("Throwing away", e)
            except Exception as e:
                print(req.infohash.hex(), type(e), e, req.source[0])
                traceback.print_exc()
                req.fails += 1
                if req.fails < 3:
                    requests.put(req)
                else:
                    print("Connection failed", req.source[0])
            sleep(0.5)


class DHTClient(Thread):
    def __init__(self, max_nodes):
        Thread.__init__(self)
        self.setDaemon(True)
        self.max_nodes = max_nodes
        self.nid = random_id()
        self.nodes = Nodes(max_nodes)

    def send_krpc(self, msg, address):
        try:
            self.socket.sendto(bencode(msg), address)
        except Exception as e:
            if not isinstance(e, KeyboardInterrupt):
                print(e)
            else:
                raise

    def send_find_node(self, address, nid=None):
        nid = get_neighbor(nid, self.nid) if nid else self.nid
        tid = entropy(TID_LENGTH)
        msg = {
            "t": tid,
            "y": "q",
            "q": "find_node",
            "a": {
                "id": nid,
                "target": random_id()
            }
        }
        self.send_krpc(msg, address)

    def bootstrap(self):
        for address in BOOTSTRAP_NODES:
            self.send_find_node(address)

    def rejoin_dht(self):
        if len(self.nodes) == 0:
            self.bootstrap()
        timer(REJOIN_DHT_INTERVAL, self.rejoin_dht)

    def auto_send_find_node(self):
        wait = 5.0 * (8 ** (len(self.nodes) / self.max_nodes))
        while True:
            for node in self.nodes:
                self.send_find_node((node.ip, node.port), node.nid)
            sleep(wait)

    def process_find_node_response(self, msg, address):
        nodes = decode_nodes(msg[b"r"][b"nodes"])
        total = 0
        for node in nodes:
            (nid, ip, port) = node
            if len(nid) != 20 or ip == self.bind_ip or port < 1 or port > 65535:
                continue
            else:
                node = self.nodes[ip]
                if node:
                    node.last_contacted = time()
                    continue
            self.nodes.append(Node(nid, ip, port, time()))
            total += 1


class DHTServer(DHTClient):
    def __init__(self, master, bind_ip, bind_port, max_nodes):
        DHTClient.__init__(self, max_nodes)

        self.master = master
        self.bind_ip = bind_ip
        self.bind_port = bind_port

        self.actions_map = {
            b"get_peers": self.on_get_peers_request,
            b"announce_peer": self.on_announce_peer_request,
        }

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.socket.bind((self.bind_ip, self.bind_port))

        timer(REJOIN_DHT_INTERVAL, self.rejoin_dht)

    def run(self):
        self.rejoin_dht()
        while True:
            try:
                (data, address) = self.socket.recvfrom(65536)
                msg = bdecode(data)
                self.on_message(msg, address)
            except Exception as e:
                if not isinstance(e, KeyboardInterrupt):
                    print(e)
                else:
                    raise

    def on_message(self, msg, address):
        try:
            if msg[b"y"] == b"r":
                if b"nodes" in msg[b"r"]:
                    self.process_find_node_response(msg, address)
            elif msg[b"y"] == b"q":
                try:
                    self.actions_map[msg[b"q"]](msg, address)
                except KeyError:
                    self.play_dead(msg, address)
        except KeyError:
            print("on_message confusion", msg)

    def on_get_peers_request(self, msg, address):
        try:
            infohash = msg[b"a"][b"info_hash"]
            tid = msg[b"t"]
            nid = msg[b"a"][b"id"]
            token = infohash[:TOKEN_LENGTH]
            msg = {
                "t": tid,
                "y": "r",
                "r": {
                    "id": get_neighbor(infohash, self.nid),
                    "nodes": "",
                    "token": token
                }
            }
            self.send_krpc(msg, address)
        except KeyError:
            print("Malformed data in on_get_peers_request", msg)

    def on_announce_peer_request(self, msg, address):
        try:
            infohash = msg[b"a"][b"info_hash"]
            token = msg[b"a"][b"token"]
            nid = msg[b"a"][b"id"]
            tid = msg[b"t"]
            if infohash[:TOKEN_LENGTH] == token:
                if b"implied_port" in msg[b"a"] and msg[b"a"][b"implied_port"] != 0:
                    port = address[1]
                else:
                    port = msg[b"a"][b"port"]
                    if port < 1 or port > 65535:
                        print('INVALID PORT', port)
                # self.master.log(infohash, (address[0], port))
                with open("hashes.txt", "a") as f:
                    f.write(infohash.hex() + "\n")
                requests.put(Request(infohash, (address[0], port)))
            else:
                print('INVALID TOKEN')
        except Exception as e:
            if not isinstance(e, KeyboardInterrupt):
                print(e)
            else:
                raise
        finally:
            self.ok(msg, address)

    def play_dead(self, msg, address):
        try:
            tid = msg[b"t"]
            msg = {
                "t": tid,
                "y": "e",
                "e": [202, "Server Error"]
            }
            self.send_krpc(msg, address)
        except KeyError:
            pass

    def ok(self, msg, address):
        try:
            tid = msg[b"t"]
            nid = msg[b"a"][b"id"]
            msg = {
                "t": tid,
                "y": "r",
                "r": {
                    "id": get_neighbor(nid, self.nid)
                }
            }
            self.send_krpc(msg, address)
        except KeyError:
            pass


class Master(object):
    def log(self, infohash, address=("0.0.0.0", "-1")):
        print("{} from {}:{}".format(infohash.hex(), address[0], address[1]))


if __name__ == "__main__":
    dht = DHTServer(Master(), "0.0.0.0", DHT_PORT, max_nodes=1000)
    dht.start()
    fetchers = []
    for x in range(9):
        mf = MetadataFetcher()
        mf.start()
        fetchers.append(mf)
    dht.auto_send_find_node()

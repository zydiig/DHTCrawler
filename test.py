from Crypto.Random import get_random_bytes

from crawl import Bitfield


def test_bitfield(s):
    f = Bitfield(s)
    return bytes(f)


data = get_random_bytes(512)
for x in range(100):
    print(data == test_bitfield(data))
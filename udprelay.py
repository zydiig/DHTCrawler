from socketserver import TCPServer,BaseRequestHandler,ThreadingMixIn

from Crypto.Cipher import ChaCha20

SECRET=open('key').read()

class TCPHandler(BaseRequestHandler):
    def handle(self):
        client=self.request
        nonce=client.recv(8)
        cipher=ChaCha20.new(key=SECRET)


class TCPServer(TCPServer, ThreadingMixIn):
    allow_reuse_address = True
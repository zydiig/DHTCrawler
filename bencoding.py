from io import BytesIO


def read_bytestring(stream):
    bio = BytesIO()
    while True:
        byte = stream.read(1)
        if byte != b':':
            bio.write(byte)
        else:
            break
    length = int(bio.getvalue().decode("utf-8"))
    return stream.read(length)


def read_int(stream):
    bio = BytesIO()
    if stream.read(1) != b'i':
        raise ValueError('Malformed integer')
    while True:
        byte = stream.read(1)
        if byte != b'e':
            bio.write(byte)
        else:
            break
    return int(bio.getvalue().decode("utf-8"))


def read_list(stream):
    if stream.read(1) != b'l':
        raise ValueError('Malformed list')
    elements = []
    while True:
        if stream.read(1) != b'e':
            stream.seek(stream.tell() - 1)
            elements.append(read_element(stream))
        else:
            break
    return elements


def read_dict(stream):
    if stream.read(1) != b'd':
        raise ValueError('Malformed dict')
    d = {}
    while True:
        if stream.read(1) != b'e':
            stream.seek(stream.tell() - 1)
            key = read_bytestring(stream)
            val = read_element(stream)
            d[key] = val
        else:
            break
    return d


def read_element(bs):
    bio = BytesIO(bs) if isinstance(bs, bytes) else bs
    kind = bio.read(1)
    bio.seek(bio.tell() - 1)
    if kind == b'l':
        return read_list(bio)
    elif kind == b'i':
        return read_int(bio)
    elif kind == b'd':
        d = read_dict(bio)
        return d
    else:
        if 48 <= kind[0] <= 57:
            return read_bytestring(bio)
        else:
            print(bs.getvalue() if isinstance(bs, BytesIO) else bs)
            raise Exception('Unexpected element')


def bdecode(bs):
    return read_element(bs)


def write_int(stream, o):
    stream.write(b'i' + str(o).encode("utf-8") + b'e')


def write_bytestring(stream, o):
    if isinstance(o, str):
        o = o.encode('utf-8')
    stream.write(str(len(o)).encode("utf-8") + b':' + o)


def write_dict(stream, o):
    stream.write(b'd')
    for k, v in o.items():
        write_bytestring(stream, k)
        write_element(stream, v)
    stream.write(b'e')


def write_list(stream, o):
    stream.write(b'l')
    for item in o:
        write_element(stream, item)
    stream.write(b'e')


def write_element(stream, obj):
    if isinstance(obj, int):
        write_int(stream, obj)
    elif isinstance(obj, bytes) or isinstance(obj, str):
        write_bytestring(stream, obj)
    elif isinstance(obj, list):
        write_list(stream, obj)
    elif isinstance(obj, dict):
        write_dict(stream, obj)
    else:
        raise ValueError("Unsupported types")


def bencode(obj):
    bio = BytesIO()
    write_element(bio, obj)
    return bio.getvalue()

if __name__=="__main__":
    print(bdecode(b'd8:msg_typei1e5:piecei0e10:total_sizei10eeabcdefghij'))